package com.kchadaj.whatstheweather.v3.service;

import com.kchadaj.whatstheweather.v3.model.Forecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherClient {

    @GET("forecast")
    Call<Forecast> fromCoordinates(
            @Query("lat") Double latitude,
            @Query("lon") Double longitude
    );
}
