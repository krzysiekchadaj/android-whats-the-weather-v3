package com.kchadaj.whatstheweather.v3.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Forecast {

    @SerializedName("list")
    private ArrayList<ForecastData> forecastData = null;

    private City city = null;

    public ArrayList<ForecastData> getForecastData() {
        return forecastData;
    }

    public City getCity() {
        return city;
    }
}
