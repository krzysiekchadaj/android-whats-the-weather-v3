package com.kchadaj.whatstheweather.v3.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.kchadaj.whatstheweather.v3.R;
import com.kchadaj.whatstheweather.v3.model.Forecast;
import com.kchadaj.whatstheweather.v3.model.ForecastData;
import com.kchadaj.whatstheweather.v3.service.WeatherClient;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherActivity extends AppCompatActivity {

    private final static String TAG = "WeatherActivity";

    private final static String URL_BASE = "https://api.openweathermap.org/data/2.5/";
    private final static String URL_API_KEY = "--->to be replaced with key to this API<---";
    private final static String URL_UNITS = "metric";

    private final int PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 1001;

    WeatherAdapter weatherAdapter;
    ArrayList<ForecastData> mForecastData;
    FusedLocationProviderClient mFusedLocationProviderClient;
    LocationCallback mLocationCallback;

    private TextView weatherDate;
    private TextView weatherTemp;
    private TextView weatherLowTemp;
    private TextView weatherCity;
    private TextView weatherDescription;
    private ImageView weatherIcon;
    private RecyclerView weatherReports;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        weatherDate = findViewById(R.id.weather_date);
        weatherTemp = findViewById(R.id.weather_temp);
        weatherLowTemp = findViewById(R.id.weather_low_temp);
        weatherCity = findViewById(R.id.weather_city);
        weatherDescription = findViewById(R.id.weather_description);
        weatherIcon = findViewById(R.id.weather_icon);
        weatherReports = findViewById(R.id.content_weather_reports);

        mFusedLocationProviderClient = new FusedLocationProviderClient(this);

        prepareWeatherReportsAdapter();

        requestLocationPermission();
    }

    private void prepareWeatherReportsAdapter() {
        mForecastData = new ArrayList<>();
        weatherAdapter = new WeatherAdapter(mForecastData);
        weatherReports.setAdapter(weatherAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        weatherReports.setLayoutManager(layoutManager);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mLocationCallback != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_REQUEST_ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_ACCESS_FINE_LOCATION:
                if (isLocationPermitted(grantResults)) {
                    requestLocation();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_gps_required), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean isLocationPermitted(int[] grantResults) {
        if (grantResults.length > 0) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void requestLocation() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationAvailability(LocationAvailability locationAvailability) {
                    super.onLocationAvailability(locationAvailability);

                    if (!locationAvailability.isLocationAvailable()) {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_gps_off), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);

                    downloadWeatherData(locationResult.getLastLocation());
                }
            };

            mFusedLocationProviderClient.requestLocationUpdates(
                    new LocationRequest().setPriority(LocationRequest.PRIORITY_LOW_POWER),
                    mLocationCallback,
                    getMainLooper());
        }
    }

    private void downloadWeatherData(Location location) {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                        Request original = chain.request();
                        HttpUrl httpUrl = original.url();
                        HttpUrl newHttpUrl = httpUrl.newBuilder()
                                .addQueryParameter("APPID", URL_API_KEY)
                                .addQueryParameter("units", URL_UNITS)
                                .build();
                        Request.Builder requestBuilder = original.newBuilder().url(newHttpUrl);
                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        retrofit.create(WeatherClient.class);

        WeatherClient client = retrofit.create(WeatherClient.class);
        Call<Forecast> call = client.fromCoordinates(location.getLatitude(), location.getLongitude());

        call.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(@NonNull Call<Forecast> call, @NonNull Response<Forecast> response) {
                if (response.code() == 200) {
                    Forecast forecast = response.body();
                    updateUI(forecast);
                    updateCardsUI(forecast);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_response_not_ok_error), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, getString(R.string.toast_response_not_ok_error));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Forecast> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(), getString(R.string.toast_response_error), Toast.LENGTH_SHORT).show();
                Log.d(TAG, getString(R.string.toast_response_error), t);
            }
        });
    }

    private void updateUI(Forecast forecast) {
        weatherCity.setText(forecast.getCity().toString());

        ForecastData forecastData = forecast.getForecastData().get(0);
        weatherDate.setText(forecastData.printDate());
        weatherTemp.setText(forecastData.getTemperature().printTemp());
        weatherLowTemp.setText(forecastData.getTemperature().printLowTemp());
        weatherDescription.setText(forecastData.getWeatherDescription().printDescription());
        weatherIcon.setImageDrawable(getResources().getDrawable(forecastData.getWeatherDescription().getIconDrawable()));
    }

    private void updateCardsUI(Forecast forecast) {
        mForecastData.clear();
        mForecastData.addAll(forecast.getForecastData());
        weatherAdapter.notifyDataSetChanged();
    }
}
